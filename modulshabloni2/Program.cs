﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//  Я вибрав шаблон Chain of Responsibility тому що цей шаблон дозволяє впорядковано обробляти запити через послідовний ланцюг об'єктів 
//  І тут потрібно робити послідовну перевірку курсу спеціальності і прізвища. І в залежності на якому етапі сталася помилка
//  Не допускати користувача до вводу якихось данних


namespace modulshabloni2
{
    class Student
    {
        public int Course { get; set; }
        public int Specialty { get; set; }
        public string LastName { get; set; }
        public int TicketNumber { get; set; }
    }

    abstract class ExamHandler
    {
        protected ExamHandler successor;

        public void SetSuccessor(ExamHandler successor)
        {
            this.successor = successor;
        }

        public abstract void HandleRequest(Student student);
    }

    class CourseHandler : ExamHandler
    {
        public override void HandleRequest(Student student)
        {
            if (student.Course != 2 || student.Specialty != 124)
            {
                Console.WriteLine("Відмова у доступі. Курс або спеціальність не підходять.");
            }
            else if (successor != null)
            {
                successor.HandleRequest(student);
            }
        }
    }

    class LastNameHandler : ExamHandler
    {
        private string[] allowedStudents = { "Dzobak", "Vogar", "Novikova" };

        public override void HandleRequest(Student student)
        {
            if (Array.Exists(allowedStudents, s => s == student.LastName))
            {
                student.TicketNumber = new Random().Next(1, 21);
                Console.WriteLine($"Студент {student.LastName} допущений до екзамену. Номер білету: {student.TicketNumber}");
            }
            else
            {
                Console.WriteLine($"Студент {student.LastName} не допущений до екзамену.");
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {

            Student student = new Student();
            bool allowLastNameInput = false;

            Console.Write("Введіть курс: ");
            if (int.TryParse(Console.ReadLine(), out int course))
            {
                student.Course = course;

                Console.Write("Введіть спеціальність: ");
                if (int.TryParse(Console.ReadLine(), out int specialty))
                {
                    student.Specialty = specialty;

                    ExamHandler courseHandler = new CourseHandler();

                    if (student.Course == 2 && student.Specialty == 124)
                    {
                        ExamHandler lastNameHandler = new LastNameHandler();
                        courseHandler.SetSuccessor(lastNameHandler);
                        allowLastNameInput = true;
                    }
                    else
                    {
                        Console.WriteLine("Помилка: Курс або спеціальність не підходять.");
                    }

                    if (allowLastNameInput)
                    {
                        Console.Write("Введіть прізвище: ");
                        student.LastName = Console.ReadLine();

                        courseHandler.HandleRequest(student);
                    }
                }
                else
                {
                    Console.WriteLine("Помилка: Невірний формат спеціальності.");
                }
            }
            else
            {
                Console.WriteLine("Помилка: Невірний формат курсу.");
            }

            Console.ReadLine();
        }
    }
}
